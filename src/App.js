import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import data from "./Data/data.json"
import Login from "./Pages/Card"
import Signin from "./Pages/Signin"
import AppBar from '@material-ui/core/AppBar';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  
    return (
    <div>
    <AppBar position="static" >
        main
    </AppBar>
    <div className="allelem">
    {data.map((postDetail, index)=>{
      return <Login className="element" marque={postDetail.marque} name={postDetail.name} price={postDetail.price} imgurl={postDetail.imgurl}></Login>
    })}
    </div></div>
  )
}

export default App;
