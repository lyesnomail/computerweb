import React from 'react'
import "./Card.css"
export default function Card({name, price, imgurl, marque}) {
    return (
        <div class = "card">
            <div  ><img class="imgcont" src = {imgurl} ></img></div>
            <div ><h4>{name}</h4> </div>
            <div >{price} {marque} </div>       
            <div className='btn' > <button> viewmore </button></div>
        </div>    
    );
}
